# Trick-data

```
npm install trick-data
```

This npm module parses data logs from [NASA's Trick simulation program](https://github.com/nasa/trick).


```js
const trick_data = require('trick-data')
const f = await trick_data.load('log_such_cannon.trk')
console.log(f.meta)
/*
{ version: 10,
  endienness: 'L',
  parameters:
   [ { name: 'sys.exec.out.time',
       units: 's',
       type: 11,
       typename: 'Double',
       size: 8 },
     { name: 'dyn.cannon.pos[0]',
       units: 'm',
       type: 11,
       typename: 'Double',
       size: 8 },
     { name: 'dyn.cannon.pos[1]',
       units: 'm',
       type: 11,
       typename: 'Double',
       size: 8 } ] }
*/

console.log(f.data)
/*
[ { 'sys.exec.out.time': 0,
    'dyn.cannon.pos[0]': 0,
    'dyn.cannon.pos[1]': 0 },
  { 'sys.exec.out.time': 0.01,
    'dyn.cannon.pos[0]': 0.4330127018922194,
    'dyn.cannon.pos[1]': 0.24950949999999994 },
  { 'sys.exec.out.time': 0.02,
    'dyn.cannon.pos[0]': 0.8660254037844388,
    'dyn.cannon.pos[1]': 0.4980379999999999 },
  { 'sys.exec.out.time': 0.03,
    'dyn.cannon.pos[0]': 1.2990381056766582,
    'dyn.cannon.pos[1]': 0.7455854999999999 },
  ...
*/
```

## References

https://github.com/nasa/trick/wiki/Data-Record


## License

Copyright 2019 Peter Brandt

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.